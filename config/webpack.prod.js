const { merge } = require('webpack-merge')

const paths = require('./paths')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: 'production',
  devtool: false,
  output: {
    path: paths.build,
    publicPath: '/',
    filename: 'js/[name].[contenthash].bundle.js',
  },
  module: {
    rules: [{ test: /\.[jt]sx?$/, exclude: /node_modules/, use: [{
        loader: require.resolve('babel-loader'),
        options: {
          compact: false
        }
      }] }],
  },
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 0,
      filename: 'vendors.[contenthash].js',
      cacheGroups: {
        // antd/ant-design в отдельный чанк vendors.antd.[contenthash].js
        antd: {
          filename: 'vendors.antd.[contenthash].js',
          test(module) {
            return (
              module.resource &&
              (module.resource.includes('antd') || module.resource.includes('ant-design'))
            )
          },
          chunks: 'all',
          reuseExistingChunk: true,
        },
        agGrid: {
          filename: 'vendors.ag-grid.[contenthash].js',
          test(module) {
            return module.resource && module.resource.includes('ag-grid')
          },
          chunks: 'all',
          reuseExistingChunk: true,
        },
        consta: {
          filename: 'vendors.consta.[contenthash].js',
          test(module) {
            return module.resource && module.resource.includes('consta')
          },
          chunks: 'all',
          reuseExistingChunk: true,
        },
        // тут наоборот: всё, кроме antd, consta, ag-grid кладем в вендорный бандл node_modules
        defaultVendors: {
          enforce: true,
          reuseExistingChunk: true,
          test(module, chunks) {
            return (
              module.resource &&
              module.resource.includes('node_modules') &&
              !module.resource.includes('antd') &&
              !module.resource.includes('ant-design') &&
              !module.resource.includes('ag-grid') &&
              !module.resource.includes('consta')
            )
          },
        },
      },
    },
  },
})
