const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const paths = require('./paths')
const { resolve } = require('path')
module.exports = {
  entry: [paths.src + '/index.tsx'],

  output: {
    path: paths.build,
    filename: '[name].bundle.js',
    publicPath: '/',
    chunkFilename: '[id].[chunkhash].js', // todo НУЖНО?
  },

  plugins: [
    new CleanWebpackPlugin(),

    new CopyWebpackPlugin({
      patterns: [
        {
          from: paths.public,
          to: 'assets',
          globOptions: {
            ignore: ['*.DS_Store'],
          },
          noErrorOnMissing: true,
        },
      ],
    }),
    new HtmlWebpackPlugin({
      title: 'Title from webpack.common.js',
      favicon: paths.src + '/assets/images/favicon.svg',
      template: paths.src + '/index.html',
      filename: 'index.html',
    }),
  ],

  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [
          'style-loader',
          {
            loader: require.resolve('css-loader'),
            options: {
              importLoaders: 1,
              // modules: {
              //   localIdentName: '[name]__[local]___[hash:base64:5]',
              //   mode: 'local',
              // },
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.(sa|sc)ss$/,
        exclude: [resolve(__dirname, '../node_modules')],
        use: [
          'style-loader', // Creates `style` nodes from JS strings
          {
            loader: 'css-loader', // Translates CSS into CommonJS
            options: {
              importLoaders: 1,
              // modules: {
              //   localIdentName: '[name]__[local]___[hash:base64:5]',
              //   mode: 'local',
              // },
              sourceMap: true,
            }
          },  // to convert the resulting CSS to Javascript to be bundled (modules:true to rename CSS classes in output to cryptic identifiers, except if wrapped in a :global(...) pseudo class)
          { loader: 'sass-loader', options: { sourceMap: true } },  // Compiles Sass to CSS
          { loader: 'postcss-loader' },
        ]
      },

      // {
      //   test: /\.css$/i,
      //   include: [resolve(__dirname, '../node_modules')],
      //   use: [
      //     'style-loader',
      //     {
      //       loader: require.resolve('css-loader'),
      //       options: {
      //         importLoaders: 1,
      //         modules: {
      //           localIdentName: '[local]',
      //         },
      //       },
      //     },
      //     { loader: 'postcss-loader' },
      //   ],
      // },
      { test: /\.(?:ico|gif|png|jpg|jpeg|xlsx|xlsm)$/i, type: 'asset/resource' },

      { test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, type: 'asset/inline' },
    ],
  },
  resolve: {
    modules: [paths.src, 'node_modules'],
    // Позволяет импортить файлы (import) без указания расширений
    extensions: [".ts", ".tsx", ".js", ".jsx", ".css", ".sass", ".scss"],
    alias: {
      '@': paths.src,
    },
  },
}
