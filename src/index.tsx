import './index.scss'

import { presetGpnDefault, Theme } from '@consta/uikit/Theme'
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

import App from './main/App'
import {RecoilRoot} from 'recoil';

ReactDOM.render(
  <Theme preset={presetGpnDefault}>
    <BrowserRouter>
      <RecoilRoot>
        <App />
      </RecoilRoot>
    </BrowserRouter>
  </Theme>,
  document.getElementById('root'),
)
