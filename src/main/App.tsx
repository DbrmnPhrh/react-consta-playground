import './App.scss';

import * as React from 'react';
import { Route } from 'react-router';

import { Home } from './pages/Home/Home';
import { Logo } from './pages/Logo/Logo';
import { Rxjs } from './pages/Rxjs/Rxjs';
import {TopPanel} from './components/TopPanel/TopPanel';
import Sidebar from './components/Sidebar/Sidebar';
import ClassComponent from './components/ClassComponent/ClassComponent';
import {useRecoilValue} from 'recoil';
import {toggleSidebarState} from './Atoms';
import {Routes} from 'react-router-dom';

const App = () => {
  const isLoading = false;
  const isSidebarVisible = useRecoilValue(toggleSidebarState);
  const routes = [
    { path: '/', component: <Home /> },
    { path: '/logo', component: <Logo /> },
    { path: '/class', component: <ClassComponent /> },
    { path: '/rxjs1', component: <Rxjs /> },
  ]

  return (
    <div className="app-container">
      <TopPanel />
      {isSidebarVisible && <Sidebar />}
      <div className="app-container-component">
        {isLoading ? (
          <h2>=== Loader component placeholder ===</h2>
        ) : (<Routes>{
            routes.map(route => (
              <Route key={route.path} path={route.path} element={route.component} />
            ))
          }
        </Routes>)}
      </div>
    </div>
  )
}

export default App
