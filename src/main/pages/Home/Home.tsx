import '../../shared/styles/shared.scss'
import './Home.scss'

import React from 'react'
import styled from 'styled-components'
import {Link} from 'react-router-dom';

const Home = () => {
  /**
   * Способы подключения стилей в React.js https://www.youtube.com/watch?v=4BvyL5cMVwU
   * https://github.com/MicheleBertoli/css-in-js
   */

  const style = {
    color: 'tomato',
    padding: '10px 0 0 0',
  }

  const StyledHeader: any = styled.h4`color: #fff;`
  const StyledBody: any = styled.span`color: cornflowerblue;`

  return (
    <div className="home-container">
      <div className="home-container-style">
        <span>Global styles:</span>
        <Link className="link" to={'/logo'}>Logo</Link>
        <Link className='link' to={'/class'}>Class component</Link>
      </div>
      <div className="home-container-style" style={{ display: 'flex', flexDirection: 'column' }}>
        <span>Inline styles:</span>
        <span style={style}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, provident.
        </span>
      </div>
      <div className="home-container-style css-regular-container">
        <span className="css-regular-container-header">CSS Module styles:</span>
        <span className="css-regular-container-body">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, provident.
        </span>
      </div>
      <div className="home-container-style">
        <StyledHeader>Styled Components</StyledHeader>
        <StyledBody>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, quia.</StyledBody>
      </div>
    </div>
  )
}

export { Home }
