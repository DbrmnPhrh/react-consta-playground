import React from 'react';
import {fromFetch} from 'rxjs/internal/observable/dom/fetch';
import UserCard, {CardData} from './UserCard/UserCard';
import './Rxjs.scss';

const Rxjs = () => {
  const [cards, setCards] = React.useState([] as CardData[]);

  React.useEffect(() => {
    fromFetch('https://jsonplaceholder.typicode.com/users', {selector: response => response.json()})
      .subscribe((cardsData: CardData[]) => setCards(cardsData));
  }, []);

  return (
    <div className='cards-container'>
      { cards.map((m: CardData) => <UserCard card={m} key={m.id} />) }
    </div>
  );
};

export { Rxjs }
