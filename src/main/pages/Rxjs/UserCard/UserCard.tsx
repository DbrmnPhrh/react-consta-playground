import React from 'react';
import './UserCard.scss'

const UserCard = (props: { card: CardData }) => {

  const [card] = React.useState(props.card);

  return (
    <div className='card-container'>
      <span>Name: {card.name}</span>
      <span>E-mail: {card.email}</span>
    </div>
  );
};

export default UserCard;

export interface CardData {
  id:       number;
  name:     string;
  username: string;
  email:    string;
  address:  Address;
  phone:    string;
  website:  string;
  company:  Company;
}

export interface Address {
  street:  string;
  suite:   string;
  city:    string;
  zipcode: string;
  geo:     Geo;
}

export interface Geo {
  lat: string;
  lng: string;
}

export interface Company {
  name:        string;
  catchPhrase: string;
  bs:          string;
}
