import * as React from 'react';
import {useRecoilState} from 'recoil';
import logo from '../../../assets/images/logo.svg';
import {toggleSidebarState} from '../../Atoms';
import {IconAdd} from '@consta/uikit/IconAdd';
import {Button} from '@consta/uikit/Button';
import {IconRemove} from '@consta/uikit/IconRemove';
import './Logo.scss';
import Counter from '../../components/Counter/Counter';
import Button1 from '../../components/Buttons/Button1';
import ClassComponent from '../../components/ClassComponent/ClassComponent';
import Credits from '../../components/Credits/Credits';

const Logo = () => {

    const [visible, setVisible] = React.useState(false);
    const [speedValue, setSpeedValue] = React.useState(20);
    const [isSidebarVisible, setToggleSidebar] = useRecoilState(toggleSidebarState);

    const arr = [1, 2, 3, 4, 5];

    return (
        <div className="logo-container">
            <img src={logo} className="logo" alt="logo" style={{animation: `logo-spin infinite ${speedValue}s linear`}} />

            <div className="buttons-container">
              <Button iconLeft={IconAdd} onlyIcon onClick={() => setSpeedValue(speed => speed + 1)} />
              <span style={{fontSize: '12px', width: '20px', margin: '0 20px'}}>{speedValue}s</span>
              <Button iconLeft={IconRemove} onlyIcon onClick={() => setSpeedValue(speed => speed < 2 ? 1 : speed - 1)} />
            </div>

            { arr.map((m, idx) => <Counter startCount={m} key={idx} />) }

            <div className='buttons-container'>
                <Button1 view="primary" label={'SHOW'} style={{marginRight: '20px'}} onClick={() => setVisible(true)}/>
                <Button1 view="secondary" label={'HIDE'} style={{marginRight: '20px'}} onClick={() => setVisible(false)}/>
                <Button1 view="primary" label={'TOGGLE'} style={{marginRight: '20px'}} onClick={() => setVisible(visible => !visible)} />
                <Button1 view="primary" label={`${isSidebarVisible ? 'Hide' : 'Show'} sidebar`} style={{width: '150px'}} onClick={() => setToggleSidebar(visible => !visible)} />
            </div>
            {visible && <ClassComponent />}
            {visible && <Credits />}
        </div>
    )
}

export { Logo }
