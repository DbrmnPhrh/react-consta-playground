import React, {useEffect, useRef, useState} from 'react';
import {MenuItem} from './menuItems';
import Dropdown from './Dropdown';
import './Sidebar.scss';
import {Link} from 'react-router-dom';
import {IconArrowRight} from '@consta/uikit/IconArrowRight';

const SidebarItems = ({ item, depthLevel }: SidebarItemsProps) => {
    const [dropdown, setDropdown] = useState(false);
    let liRef = useRef<HTMLLIElement>(null);

    useEffect(() => {
      const handler = (event: MouseEvent | TouchEvent) => {
        event.preventDefault();
        if (event.target instanceof HTMLElement && dropdown && !liRef.current?.contains(event.target)) {
          setDropdown(false);
        }
      };
      document.addEventListener("mousedown", handler);
      return () => document.removeEventListener("mousedown", handler);
    }, [dropdown]);

    const onMouseClick = () => window.innerWidth > 960 && setDropdown(true);

    return (
      <li
        className="menu-items"
        ref={liRef}
        onClick={onMouseClick}
      >
        {item.submenu
          ? (<>
               <div className='button-item'>
                 <button type='button'>{item.title}</button>
                 <IconArrowRight size="m" />
               </div>
               <Dropdown
                   submenus={item.submenu}
                   dropdown={dropdown}
                   depthLevel={depthLevel}
               />
             </>)
          : (<Link to={item.url as string} relative={'route'}>{item.title}</Link>)
        }
      </li>
    );
};

export default SidebarItems;

interface SidebarItemsProps {
  item: MenuItem;
  depthLevel: number;
}
