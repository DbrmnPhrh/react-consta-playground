export const menuItems: MenuItem[] = [
  {
    title: 'Home',
    url: ''
  },
  {
    title: 'React features',
    submenu: [
      {
        title: 'Components',
        submenu: [
          {
            title: 'ClassComponent',
            url: 'class',
          },
          {
            title: 'Logo',
            url: 'logo',
          },
        ],
      }
    ],
  },
  {
    title: 'Services',
    submenu: [
      {
        title: 'web design',
      },
      {
        title: 'web development',
        submenu: [
          {
            title: 'Frontend',
          },
          {
            title: 'Backend',
            submenu: [
              {
                title: 'NodeJS',
              },
              {
                title: 'PHP',
              },
            ],
          },
        ],
      },
      {
        title: 'SEO',
      },
    ],
  },
  {
    title: 'RxJS',
    submenu: [
      {
        title: 'RxJS example 1',
        url: 'rxjs1',
      },
      {
        title: 'RxJS example 2',
        url: 'rxjs2',
      },
    ],
  },
];

export interface MenuItem {
  title: string;
  submenu?: MenuItem[];
  url?: string;
}
