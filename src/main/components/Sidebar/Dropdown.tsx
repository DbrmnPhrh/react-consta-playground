import React from 'react';
import SidebarItems from './SidebarItems';
import {MenuItem} from './menuItems';

const Dropdown = ({ submenus, dropdown, depthLevel }: DropdownProps) => {
    depthLevel = depthLevel + 1;
    const dropdownClass = depthLevel > 1 ? "dropdown-submenu" : "";
    return (
        <ul className={`dropdown ${dropdownClass} ${dropdown ? "show" : ""}`}>
            {submenus.map((submenu: MenuItem, index: number) => (
                <SidebarItems item={submenu} key={index} depthLevel={depthLevel} />
            ))}
        </ul>
    );
};

export default Dropdown;

interface DropdownProps {
    submenus: MenuItem[];
    dropdown: boolean;
    depthLevel: number;
}
