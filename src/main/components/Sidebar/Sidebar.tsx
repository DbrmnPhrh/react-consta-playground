import React from 'react';
import {MenuItem, menuItems} from './menuItems';
import SidebarItems from './SidebarItems';
import './Sidebar.scss';
import {useRecoilState} from 'recoil';
import {toggleSidebarState} from '../../Atoms';

const Sidebar = () => {
  const [, setToggleSidebar] = useRecoilState(toggleSidebarState);

  return (
    <>
      <nav className="sidebar-container">
        <ul className="sidebar-container-menu">
          {menuItems.map((menuItem: MenuItem, index) => {
            const depthLevel = 0;
            return <SidebarItems item={menuItem} key={index} depthLevel={depthLevel} />
          })}
        </ul>
      </nav>
      <div className="sidebar-background" onClick={() => setToggleSidebar(visible => !visible)}></div>
    </>
  )
};

export default Sidebar;
