import React from 'react';

const Credits = () => {

    React.useEffect(() => {
        console.log('Counter component DID MOUNTED!');
        return () => {
            console.log('Counter component WILL UNMOUNTED!')
        };
    }, []);

    return (
        <div>
            <p> Edit <code>src/App.js</code> and save to reload </p>

            <a className="logo-link"
               href="https://reactjs.org"
               target="_blank"
               rel="noopener noreferrer">Learn React</a>
        </div>
    );
};

export default Credits;
