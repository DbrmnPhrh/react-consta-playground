import React from 'react';
import {Button} from '@consta/uikit/Button';
import {IconAdd} from '@consta/uikit/IconAdd';
import {Badge} from '@consta/uikit/Badge';
import {IconCancel} from '@consta/uikit/IconCancel';

const Counter = (props: CounterProps) => {

    const [count, setCount] = React.useState(props.startCount);

    /** Изменилось именно значение счётчика */
    React.useEffect(() => console.log('Count changed!'), [count]);

    /** Произошли любые изменения */
    React.useEffect(() => console.log('Some changes occurred!'));

    return (
        <div style={{display: 'flex'}}>
            <Button iconLeft={IconAdd} onlyIcon onClick={() => setCount(count + 1)} />
            <Badge size="s" form="round" status="error" label={count.toString()} style={{left: '-10px'}} />
            <Button iconRight={IconCancel} onClick={() => setCount(0)} />
        </div>
    )
}

export default Counter;

export interface CounterProps {
   startCount: number;
}
