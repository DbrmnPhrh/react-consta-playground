import React, { FC } from 'react';
import {useRecoilState} from 'recoil';
import {toggleSidebarState} from '../../Atoms';
import Button1 from '../Buttons/Button1';
import './TopPanel.scss';
import {IconAlignJustify} from '@consta/uikit/IconAlignJustify';

interface TopPanelProps {}

const TopPanel: FC<TopPanelProps> = () => {

  const [, setToggleSidebar] = useRecoilState(toggleSidebarState);

  return (
    <div className='app-container-top-panel'>
      <Button1 view="primary" style={{marginLeft: '10px'}} iconLeft={IconAlignJustify} onlyIcon onClick={() => setToggleSidebar(visible => !visible)} />
    </div>
  )
};

export { TopPanel }
