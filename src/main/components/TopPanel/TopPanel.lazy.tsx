import React, { lazy, Suspense } from 'react';

const LazyTopPanel = lazy(() => import('./TopPanel'));

const TopPanel = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode; }) => (
  <Suspense fallback={null}>
    <LazyTopPanel {...props} />
  </Suspense>
);

export default TopPanel;
