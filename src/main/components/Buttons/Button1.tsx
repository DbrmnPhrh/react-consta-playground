import React from 'react';
import {Button} from '@consta/uikit/Button';
import {ButtonPropSize, ButtonPropView} from '@consta/uikit/__internal__/src/components/Button/Button';

/**
 * Компонент Button1 возвращает компонент Button из Consta Design с переданными пропсами
 */

const Button1 = (props: ButtonParams) => {
    const { view, ...other } = props; // проперти view можно выделить и как-то обработать отдельно
    const size: ButtonPropSize = view === 'primary' ? 'm' : 's';
    return (
        <Button view={view} size={size} {...other} />
    );
};

export default Button1;

export interface ButtonParams {
    view: ButtonPropView;
    [x: string]: any;
}
