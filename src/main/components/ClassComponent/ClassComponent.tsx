import React, {Component} from 'react';
import {Button} from '@consta/uikit/Button';
import './ClassComponent.scss';

class MyProps {
}

class MyState {
}

class ClassComponent extends Component<MyProps, MyState> {

    constructor(props: MyProps) {
        super(props);
    }

    state = {
      numbers: [1, 2, 3]
    }

    componentDidMount = () => {
        console.log('test did mount!');
    }

    changeNumbers = () => {
        this.setState({
            numbers: this.state.numbers.map(m => m * 2)
        })
    }

    componentDidUpdate(prevProps: unknown, prevState: Readonly<{numbers: number[]}>) {
        const isChangeOccurred = this.state.numbers.some(e => prevState.numbers.every(n => n !== e));
        if (isChangeOccurred) {
            console.log('test did update');
            console.log('prevProps, prevState', prevProps, prevState);
            console.log('nextState', this.state.numbers);
        }
    }

    render() {
        return (
            <div className="test-class-container">
                <span>Test class component {this.state.numbers}</span>
                <Button view="primary" label={'change'} onClick={this.changeNumbers} />
            </div>
        )
    }

    componentWillUnmount() {
        console.log('test will unmount!');
    }
}

export default ClassComponent;
