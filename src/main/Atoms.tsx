import { atom } from 'recoil';

export const toggleSidebarState = atom({
  key: 'toggleSidebar',
  default: false,
});
